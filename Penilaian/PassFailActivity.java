package Penilaian;

public class PassFailActivity extends GradedActivity {
    private double minimumPassingScore;

    public PassFailActivity(double min) {
        minimumPassingScore = min;
    }
    public char getGrade() {
        char grade;
        if (getScore() >= minimumPassingScore) {
            grade = 'P';
        } else {
            grade = 'F';
        }
        return grade;
    }
}
