package Penilaian;

public class PassFailExam extends PassFailActivity {
    int numberOfQuestions;
    int numberMissed;
    double pointsPerQuestion;

    public PassFailExam(int questions, int missed, double minimumPassingScore) {
        super(minimumPassingScore);
        numberOfQuestions = questions;
        numberMissed = missed;
        pointsPerQuestion = 100 / questions;
        double numericScore = 100 - (missed * pointsPerQuestion);
        setScore(numericScore);
    }

    public double getPointsPerQuestion() {
        return pointsPerQuestion;
    }

    public int getNumberMissed() {
        return numberMissed;
    }
}