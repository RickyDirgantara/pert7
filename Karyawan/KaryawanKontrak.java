package Karyawan;

public class KaryawanKontrak extends Karyawan {
    int upahHarian;

    public KaryawanKontrak(String nama, int umur, int jumlahAnak, int upahHarian) {
        super(nama, umur, jumlahAnak);
        this.upahHarian = upahHarian;
    }

    public double hitungTotalUpah(int jumlahHariMasuk) {
        return upahHarian * jumlahHariMasuk * getJumlahAnak() * 0.1;
    }
}

