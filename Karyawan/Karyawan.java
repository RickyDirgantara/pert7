package Karyawan;
public class Karyawan {
    String nama;
    int umur;
    int jumlahAnak;

    public Karyawan(String nama, int umur, int jumlahAnak) {
        this.nama = nama;
        this.umur = umur;
        this.jumlahAnak = jumlahAnak;
    }

    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }

    public int getJumlahAnak() {
        return jumlahAnak;
    }
}