package Karyawan;

public class KaryawanTetap extends Karyawan {
    double gajiPokok;

    public KaryawanTetap(String nama, int umur, int jumlahAnak, double gajiPokok) {
        super(nama, umur, jumlahAnak);
        this.gajiPokok = gajiPokok;
    }

    public double hitungTotalGaji() {
        return 0.1 * gajiPokok * getJumlahAnak();
    }
}
