package Penilaian;

public class FinalExam extends GradedActivity{
    int numberOfQuestions;
    int numberMissed;
    double pointsPerQuestion;

    public FinalExam(int questions, int missed) {
        numberOfQuestions = questions;
        numberMissed = missed;
        pointsPerQuestion = 100 / numberOfQuestions;
    }

    public double getPointsPerQuestion() {
        return pointsPerQuestion;
    }

    public int getNumberMissed() {
        return numberMissed;
    }

    public double getScore() {
        return 100 - (numberMissed * pointsPerQuestion);
    }
}
