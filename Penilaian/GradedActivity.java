package Penilaian;

public class GradedActivity {
    double score;
    String name;

    public GradedActivity() {}

    public GradedActivity(String n) {
        name = n;
    }

    public void setScore(double s) {
        score = s;
    }

    public double getScore() {
        return score;
    }

    public char getGrade() {
        char grade;
        if (score >= 85) {
            grade = 'A';
        } else if (score >= 80) {
            grade = 'B';
        } else if (score >= 65) {
            grade = 'C';
        } else if (score >= 60) {
            grade = 'D';
        } else {
            grade = 'F';
        }
        return grade;
    }
}
