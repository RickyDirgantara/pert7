class Mobil2 {
    int speed = 0;
    int gear = 0;

    void changeGear(int newValue) {
        gear = gear + newValue;
        System.out.println("\nGear: " + gear);
    }

    void speedUp(int increment) {
        speed = speed + increment;
        System.out.println("\nSpeed: " + speed);
    }
  
}
class Mobil2BMW extends Mobil2 {
    int nitrous;
    String brand = "BMW";

    void hidupkanMobil() {
        System.out.println("Engine Start");
    }
    void nontonTV() {
        System.out.println("TV dihidupkan");
        System.out.println("Mencari Channel");
        System.out.println("Gambar Ditampilkan");
    }
    void matikanMobil() {
        System.out.println("Mobil dimatikan");
    }

    void nitrous(int newValue) {
    nitrous = newValue;
        System.out.println("Total NoS: " + nitrous);
    }
}
class Mobil2BMWDemo {
    public static void main(String[] args) {
        Mobil2BMW mobil = new Mobil2BMW();
        System.out.println("Brand: " + mobil.brand);
        mobil.hidupkanMobil();
        mobil.nontonTV();
        mobil.changeGear(1);
        mobil.matikanMobil();
    }
}
